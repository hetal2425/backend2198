import requests
import json
from datetime import datetime
def training():
	from boto3.session import Session
	import boto3

	# ACCESS_KEY = 'AKIAYDD4JBKG7DQIQGUJ'
	# SECRET_KEY = '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'

	# session = Session(aws_access_key_id=ACCESS_KEY,
	# 			aws_secret_access_key=SECRET_KEY)
	# s3 = session.resource('s3')
	# your_bucket = s3.Bucket('sagemaker-studio-556458904205-a1qtpu3eo5')

	# s3= session.client('s3')
	# s3.download_file('sagemaker-studio-556458904205-a1qtpu3eo5','channelprediction.xlsx','channelprediction.xlsx')

	import pandas as pd

	read_file = pd.read_excel ("channelprediction.xlsx")

	read_file.to_csv ("channelprediction.csv", 
					index = None,
					header=True)

	df = pd.DataFrame(pd.read_csv("channelprediction.csv"))

	df


	import pandas as pd
	import numpy as np
	import matplotlib.pyplot as plt
	import seaborn as sns
	import warnings
	warnings.filterwarnings('ignore')

	#!pip install xgboost
	from xgboost import XGBClassifier
	from sklearn.model_selection import train_test_split
	from sklearn.metrics import accuracy_score
	from sklearn.ensemble import GradientBoostingClassifier
	from sklearn.ensemble import RandomForestClassifier
	from sklearn.neighbors import KNeighborsClassifier
	from sklearn.naive_bayes import GaussianNB
	from sklearn.model_selection import cross_validate, GridSearchCV, train_test_split
	from sklearn.metrics import classification_report, confusion_matrix

	#Clear out HTML characters
	from html.parser import HTMLParser
	df = pd.read_csv(r"channelprediction.csv")
	df['NoticeDetail']=HTMLParser().unescape(df['NoticeDetail'])
	#Remove special characters
	df['NoticeDetail'] = df['NoticeDetail'].str.replace('\W', '')


	df=df[['NoticeDetail','Time_Delivered','Weekday_weekend','Channel_name','Responded','UserID']]
	print(df['UserID'][0])
	df=df.dropna(subset=['Channel_name'])#Null removed

	# from google.colab import drive
	# drive.mount('/content/drive')

	# Import stopwords with nltk.
	import nltk
	nltk.download("stopwords")
	from nltk.corpus import stopwords
	stop = stopwords.words('english')
	#remove rows with null values
	df = df[df['NoticeDetail'].notnull()]
	df['NoticeDetail'] = df['NoticeDetail'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop)]))

	#pip install sentence_transformers
	from sentence_transformers import SentenceTransformer
	model = SentenceTransformer('distilbert-base-nli-stsb-mean-tokens')
	df['NoticeDetail'] = model.encode(df['NoticeDetail'].values)

	df.tail(300)

	#Making a column for hour from Time Delivered

	import time
	import datetime
	import calendar

	df['TIME'] = pd.to_datetime(df['Time_Delivered']).dt.time
	df['hour'] = pd.to_datetime(df['TIME'], format='%H:%M:%S').dt.hour

	#Making a column for hour from Time Delivered
	#df['TIME'] = pd.to_datetime(df['Time_Delivered'], format= '%H:%M:%S')

	#df['hour'] = df['TIME'].dt.hour

	#Storing original values as a copy
	df['Weekday_weekend_or']=df['Weekday_weekend']
	df['Channel_name_or']=df['Channel_name']
	categ = ['Weekday_weekend','Channel_name']
	from sklearn.preprocessing import LabelEncoder
	le = LabelEncoder()
	df[categ]=df[categ].apply(le.fit_transform)
	X=df[['NoticeDetail','Weekday_weekend','Responded','hour']]
	y = df[['Channel_name']]
	X_train, X_test, y_train, y_test = train_test_split(X,y,test_size = 0.20)

	df.head()

	from sklearn.model_selection import train_test_split
	from sklearn.metrics import accuracy_score
	from sklearn.ensemble import GradientBoostingClassifier
	from sklearn.naive_bayes import GaussianNB
	from sklearn.metrics import classification_report, confusion_matrix

	from sklearn.linear_model import LogisticRegression
	Logistic_model =  LogisticRegression()
	Logistic_model.fit(X_train, y_train)
	y_pred = Logistic_model.predict(X_test)
	from sklearn.metrics import classification_report, confusion_matrix
	print(confusion_matrix(y_test,y_pred))
	print('---------------------------')
	print(classification_report(y_test,y_pred))

	GB_model =  GradientBoostingClassifier()
	GB_model.fit(X_train, y_train)
	y_pred =GB_model.predict(X_test)
	from sklearn.metrics import classification_report, confusion_matrix
	print(confusion_matrix(y_test,y_pred))
	print('---------------------------')
	print(classification_report(y_test,y_pred))

	# save the model to disk
	import pickle
	filename = 'GB_model.sav'
	pickle.dump(GB_model, open(filename, 'wb'))

	# load the model from disk
	loaded_model = pickle.load(open(filename, 'rb'))
	result = loaded_model.score(X_test, y_test)
	print(result)

	arr=[[-0.491940,	0,	1,	11]]

	model_predict=loaded_model.predict(arr)
	print(le.inverse_transform(model_predict))


	categ = ['Weekday_weekend','Channel_name','Responded','hour']
	from sklearn.preprocessing import LabelEncoder
	le = LabelEncoder()
	df[categ]=df[categ].apply(le.fit_transform)
	df.head()
	# print("ended")
	user = ""
	for i in df.index:
		if user == df['UserID'][i]:
			continue

		else:
			arr = []
			arr.append(df['NoticeDetail'][i])
			arr.append(df['Weekday_weekend'][i])
			arr.append(df['Responded'][i])
			arr.append(df['hour'][i])
			model_predict=loaded_model.predict([arr])
			val = le.inverse_transform(model_predict)[0]
			categ = ['Weekday_weekend','Channel_name','Responded','hour']
			from sklearn.preprocessing import LabelEncoder
			le = LabelEncoder()
			df[categ]=df[categ].apply(le.fit_transform)
			from datetime import datetime
			df.head()
			user = str(df['UserID'][i])
			print(user)
			date = datetime.now().date()
			day = calendar.day_name[date.weekday()][:2]
			channel_data = ["EchoApp","Whatsapp","Email"]

			time_00 = datetime.strptime('00:00', '%H:%M').time()
			time_06 = datetime.strptime('06:00', '%H:%M').time()
			time_12 = datetime.strptime('12:00', '%H:%M').time()
			time_18 = datetime.strptime('18:00', '%H:%M').time()

			if time_00 <= datetime.now().time() <= time_06:
				col_name = day+"_24_6"
			if time_06 <= datetime.now().time() <= time_12:
				col_name = day+"_6_12"
			if time_12 <= datetime.now().time() <= time_18:
				col_name = day+"_12_18"
			if time_18 <= datetime.now().time() <= time_00:
				col_name = day+"_18_24"
			
			print(col_name)

			header = {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmltYXJ5c2lkIjoiYWNmYzM5ZDYtN2NjYS00MTdkLWJkYWQtMDk2YzA3OWEyM2VjIiwicHJpbWFyeWdyb3Vwc2lkIjoiOTQzOSIsImVtYWlsIjoicHJhc2hhbnQuY2hhdmFuQGVjaG9jb21tdW5pY2F0b3IuY29tIiwibmFtZSI6IlBQUFciLCJUaW1lWm9uZU9mZnNldCI6IjMzMCIsIkNsaWVudE5hbWUiOiJTaHJlZVRycnIiLCJyb2xlIjoiVXNlciIsIm5iZiI6MTYzMTY4NzY4OCwiZXhwIjoxNjMyMjkyNDg4LCJpYXQiOjE2MzE2ODc2ODh9.LQ_pQCQyY7xQM-U7WNezDGIDhczLLp77_rr6feY1UL0','content-type': 'application/json'}

			print(user)

			payload = {
				"UserID" : user,
				"Slot" : col_name,
				"Value" : channel_data[val]
			} 
		
			response = requests.post('https://devapi.echocommunicator.com/api/Dashboard/AddUpdateChannelPrediction', data = json.dumps(payload), headers=header, )
			# return response

print(training())

# import requests
# import json

# header = {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmltYXJ5c2lkIjoiYWNmYzM5ZDYtN2NjYS00MTdkLWJkYWQtMDk2YzA3OWEyM2VjIiwicHJpbWFyeWdyb3Vwc2lkIjoiOTQzOSIsImVtYWlsIjoicHJhc2hhbnQuY2hhdmFuQGVjaG9jb21tdW5pY2F0b3IuY29tIiwibmFtZSI6IlBQUFciLCJUaW1lWm9uZU9mZnNldCI6IjMzMCIsIkNsaWVudE5hbWUiOiJTaHJlZVRycnIiLCJyb2xlIjoiVXNlciIsIm5iZiI6MTYzMTY4NzY4OCwiZXhwIjoxNjMyMjkyNDg4LCJpYXQiOjE2MzE2ODc2ODh9.LQ_pQCQyY7xQM-U7WNezDGIDhczLLp77_rr6feY1UL0','content-type': 'application/json'}

# payload = {
# 	"UserID" : "E7CA2222-182C-4374-86E7-FB1130AEE5AB",
# 	"Slot" : "MO_6_12",
# 	"Value" : "Whatsapp"
# } 

# response = requests.post('https://devapi.echocommunicator.com/api/Dashboard/AddUpdateChannelPrediction', data = json.dumps(payload), headers=header, )
# print(str(response.json()))