﻿namespace EchoChat.Constants
{
    public class ChannelMaster
    {
        public const string SMSChannel = "SMSChannel";
        public const string EmailChannel = "EmailChannel";
        public const string FacebookChannel = "FacebookChannel";
        public const string PushNotificationChannel = "PushNotificationChannel";
        public const string SlackChannel = "SlackChannel";
        public const string TelegramChannel = "TelegramChannel";
        public const string ViberChannel = "ViberChannel";
        public const string WhatsappChannel = "WhatsappChannel";
        public const string WhatsappKarixChannel = "WhatsappKarixChannel";
    }
}
