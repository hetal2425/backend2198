﻿using iText.Html2pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EchoChat.Helper
{
    public class PDFHelper
    {
        public static MemoryStream GetPdf(string title, string htmlDetails, string text, string date)
        {
            string html = AWSHelper.AnalyzeText(title, htmlDetails, text, date);


            ConverterProperties converterProperties = new ConverterProperties();
            MemoryStream stream = new MemoryStream();
            HtmlConverter.ConvertToPdf(html, stream, converterProperties);
            return stream;
        }
    }
}
