﻿using EchoChat.Models;
using EchoChat.Models.WebHookModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EchoChat.Repository
{
    public interface IHookRepository
    {
        void InsertSESEvents(AWSSESEventMessege message);
        NoticeDTO InsertReceiveEmail(AWSReceiveEmailData message, string apiURL);
        void InsertWhatsAppEvents(string recipient_id, string id, string status, DateTime _timeStamp);
        NoticeDTO InsertIncomingWhatsApp(WAMessage msg, string apiURL);
        void InsertSMSEvents(string Status, string Operator, string Circle, string Dest, string Dtime, string Stime, string MID, string Reason, string Send);
        void InsertLinkedHelperData(LinkedHelperData data);
        NoticeDTO InsertReceiveSlack(string eventType, string msgId, string refMessageId, string from, string to, DateTime timestamp, string message);
    }
}
