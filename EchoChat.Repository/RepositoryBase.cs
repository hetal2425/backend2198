﻿using EchoChat.DB;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EchoChat.Repository
{
    public class RepositoryBase : IRepositoryBase
    {
        public SPToCoreContext _Context { get; set; }
        public RepositoryBase(SPToCoreContext spToCoreContext)
        {
            _Context = spToCoreContext;
        }

        public bool Add<T>(T entity) where T : class
        {
            try
            {
                this._Context.Entry(entity).State = EntityState.Added;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update<T>(T entity) where T : class
        {
            try
            {
                this._Context.Entry(entity).State = EntityState.Modified;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Remove<T>(T entity) where T : class
        {
            this._Context.Entry(entity).State = EntityState.Deleted;
            return true;
        }

        public IQueryable<T> Get<T>(Expression<Func<T, bool>> condition) where T : class
        {
            try
            {
                return this._Context.Set<T>().Where(condition);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IQueryable<T> GetWith<T>(string includeEntities, Expression<Func<T, bool>> condition) where T : class
        {
            try
            {
                return this._Context.Set<T>().Include(includeEntities).Where(condition);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IQueryable<T> Get<T>() where T : class
        {
            try
            {
                return this._Context.Set<T>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveChanges()
        {
            try
            {
                this._Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ExecuteSqlCommand(string sql)
        {
            try
            {
                return this._Context.Database.ExecuteSqlRaw(sql);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddLog(string logType, string moduleType, int moduleID, string details, Guid createdBy, bool saveChanges = false)
        {
            try
            {
                Log l = new Log
                {
                    Type = logType,
                    Module = moduleType,
                    ModuleId = moduleID,
                    Details = details,
                    CreatedBy = createdBy,
                    CreatedDate = DateTime.UtcNow
                };

                this.Add<Log>(l);

                if (saveChanges)
                    this._Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            this._Context.Dispose();
        }
    }
}
