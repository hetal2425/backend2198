﻿using AutoMapper;
using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.Repository
{
    public class ClientRepository : IClientRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public ClientRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }

        public bool CheckClientIsAlreadyRegistered(string email, string mobileNo)
        {
            return _repositoryBase.Get<Client>(x => x.Email == email || x.MobileNo == mobileNo).Any();
        }

        public List<AppSettingsVM> GetAppSettingsByEntityID(int entityID)
        {
            return _repositoryBase.Get<EchoChat.DB.AppSetting>(a => a.EntityId.Equals(entityID))
                    .Select(a => new AppSettingsVM() { EntityID = a.EntityId ?? 0, EntityName = a.EntityName, Key = a.Key, Details = a.Details, Value = a.Value, Value1 = a.Value1, Value2 = a.Value2, Value3 = a.Value3, Value4 = a.Value4, Value5 = a.Value5, Value6 = a.Value6, Value7 = a.Value7 }).ToList();
        }
        public string GetClientEmailById(int Id)
        {
            var clientObj = _repositoryBase.Get<Client>(a => a.Id == Id).FirstOrDefault();
            return clientObj?.Email;
        }
        public int SaveClient(ClientDTO client)
        {
            Client c = new Client();
            c.OrganizationName = client.OrganizationName;
            c.MobileNo = client.MobileNo;
            c.ContactPersonName = client.ContactPersonName;
            c.Email = client.Email;
            c.ClientLogo = client.ClientLogo;
            c.IsDeleted = false;
            c.IsTrial = true;
            c.CreateDate = DateTime.UtcNow;
            _repositoryBase.Add<Client>(c);
            _repositoryBase.SaveChanges();

            User u = new User();
            u.UserId = Guid.NewGuid();
            u.ClientId = c.Id;
            u.EmailId = c.Email;
            u.MobileNo = c.MobileNo;
            u.FirstName = c.ContactPersonName;
            u.Password = client.Password;
            u.IsDeleted = false;
            u.CreatedBy = u.UserId;
            u.CreateDate = DateTime.UtcNow;

            _repositoryBase.Add<User>(u);
            _repositoryBase.SaveChanges();

            return c.Id;
        }
    }
}
