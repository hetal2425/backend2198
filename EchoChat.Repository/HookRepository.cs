﻿using AutoMapper;
using EchoChat.DataParser;
using EchoChat.DB;
using EchoChat.Helper;
using EchoChat.Models;
using EchoChat.Models.WebHookModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EchoChat.Repository
{
    public class HookRepository : IHookRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public HookRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }

        public void InsertSESEvents(AWSSESEventMessege message)
        {
            try
            {
                _repositoryBase.ExecuteSqlCommand(string.Format("exec usp_InsertSESEvents '{0}', '{1}', '{2}', '{3}', '{4}', '{5}'", message.eventType ?? string.Empty, message.mail.messageId ?? string.Empty, message.mail.source ?? string.Empty, message.mail.destination[0] ?? string.Empty, message.open != null ? message.open.ipAddress : string.Empty, message.open != null ? message.open.timestamp : message.delivery != null ? message.delivery.timestamp : message.mail.timestamp));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NoticeDTO InsertReceiveEmail(AWSReceiveEmailData message, string apiURL)
        {
            try
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

                string refMsgID = message.mail.headers.Where(h => h.name == "In-Reply-To").Select(h => h.value).FirstOrDefault();
                refMsgID ??= string.Empty;
                refMsgID = refMsgID.Trim('<').Trim('>').Replace("@us-west-2.amazonses.com", string.Empty);

                if (string.IsNullOrEmpty(message.mail.source) || message.mail.destination == null || message.mail.destination.Count == 0 ||
                    !regex.Match(message.mail.destination[0]).Success || string.IsNullOrEmpty(refMsgID))
                {
                    return null;
                }

                string content = EmailContentParser.ParseReceivedEmailContent(message.content);

                var result = _repositoryBase._Context.usp_InsertReceiveEmailAsync(message.notificationType ?? string.Empty, message.mail.messageId ?? string.Empty, refMsgID ?? string.Empty, message.mail.source ?? string.Empty, message.mail.destination[0] ?? string.Empty, message.mail.timestamp, content ?? string.Empty);
                result.Wait();
                List<usp_InsertReceiveEmailResult> data = result.Result;

                if (data != null && data.Count > 0)
                {
                    if (data[0].UserID != null && data[0].NoticeID.HasValue)
                    {
                        NoticeDTO notice = new NoticeDTO();
                        notice.Createdby = data[0].UserID;
                        notice.NoticeDetail = content + "\n --Sent via Email";
                        notice.ParentID = data[0].NoticeID.ToString();
                        notice.GroupID = data[0].GroupID.ToString();
                        return notice;
                        //string userAgent = "Webhook";

                        //Dictionary<string, object> postParameters = new Dictionary<string, object>();
                        //postParameters.Add("Createdby", notice.Createdby);
                        //postParameters.Add("NoticeDetail", notice.NoticeDetail + "\n --Sent via Email");
                        //postParameters.Add("ParentId", notice.ParentId);
                        //postParameters.Add("GroupID", notice.GroupID);


                        //HttpWebResponse webResponse = MultiformHelper.MultipartFormPost(apiURL + "api/Notice/AddNotice", apiURL + "api/Auth/Login", userAgent, postParameters, "", "");
                        //// Process response  
                        //StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                        //string returnResponseText = responseReader.ReadToEnd();
                        //webResponse.Close();
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertWhatsAppEvents(string recipient_id, string id, string status, DateTime _timeStamp)
        {
            try
            {
                _repositoryBase.ExecuteSqlCommand(string.Format("exec usp_InsertWA '{0}', '{1}', '{2}', '{3}'", recipient_id, id, status, _timeStamp));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NoticeDTO InsertIncomingWhatsApp(WAMessage message, string apiURL)
        {
            try
            {
                var result = _repositoryBase._Context.usp_InserMsgWAAsync(message.from ?? string.Empty, message.id ?? string.Empty, message.text.body ?? string.Empty, message.Date, message.type ?? string.Empty);
                result.Wait();
                List<usp_InserMsgWAResult> data = result.Result;

                if (data != null && data.Count > 0)
                {
                    if (data[0].UserId != null && data[0].NoticeID.HasValue)
                    {
                        NoticeDTO notice = new NoticeDTO();
                        notice.Createdby = data[0].UserId;
                        notice.NoticeDetail = message.text.body + "\n --Sent via WA";
                        notice.ParentID = data[0].NoticeID.Value.ToString();
                        notice.GroupID = data[0].GroupID.ToString();

                        return notice;

                        //string userAgent = "Webhook";

                        //Dictionary<string, object> postParameters = new Dictionary<string, object>();
                        //postParameters.Add("Createdby", notice.Createdby);
                        //postParameters.Add("NoticeDetail", notice.NoticeDetail + "\n --Sent via WA");
                        //postParameters.Add("ParentId", notice.ParentId);
                        //postParameters.Add("GroupID", notice.GroupID);

                        //HttpWebResponse webResponse = MultiformHelper.MultipartFormPost(apiURL + "api/Notice/AddNotice", apiURL + "api/Auth/Login", userAgent, postParameters, "", "");
                        //// Process response  
                        //StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                        //string returnResponseText = responseReader.ReadToEnd();
                        //webResponse.Close();
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertLinkedHelperData(LinkedHelperData linkedHelperData)
        {
            try
            {
                _repositoryBase._Context.usp_InsertLinkedHelperData(linkedHelperData.member_id ?? string.Empty,
    linkedHelperData.profile_url ?? string.Empty,
    linkedHelperData.email ?? string.Empty,
    linkedHelperData.full_name ?? string.Empty,
    linkedHelperData.first_name ?? string.Empty,
    linkedHelperData.last_name ?? string.Empty,
    linkedHelperData.industry ?? string.Empty,
    linkedHelperData.address ?? string.Empty,
    linkedHelperData.birthday ?? string.Empty,
    linkedHelperData.headline ?? string.Empty,
    linkedHelperData.current_company ?? string.Empty,
    linkedHelperData.current_company_custom ?? string.Empty,
    linkedHelperData.current_company_position ?? string.Empty,
    linkedHelperData.current_company_custom_position ?? string.Empty,
    linkedHelperData.organization_1 ?? string.Empty,
    linkedHelperData.organization_url_1 ?? string.Empty,
    linkedHelperData.organization_title_1 ?? string.Empty,
    linkedHelperData.organization_location_1 ?? string.Empty,
    linkedHelperData.organization_website_1 ?? string.Empty,
    linkedHelperData.organization_2 ?? string.Empty,
    linkedHelperData.organization_title_2 ?? string.Empty,
    linkedHelperData.education_1 ?? string.Empty,
    linkedHelperData.education_degree_1 ?? string.Empty,
    linkedHelperData.phone_1 ?? string.Empty,
    linkedHelperData.phone_type_1 ?? string.Empty,
    linkedHelperData.phone_2 ?? string.Empty,
    linkedHelperData.phone_type_2 ?? string.Empty,
    linkedHelperData.messenger_1 ?? string.Empty,
    linkedHelperData.messenger_provider_1 ?? string.Empty,
    linkedHelperData.website_1 ?? string.Empty,
    linkedHelperData.third_party_email_1 ?? string.Empty,
    linkedHelperData.campaign_id ?? string.Empty,
    linkedHelperData.campaign_name ?? string.Empty);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertSMSEvents(string Status, string Operator, string Circle, string Dest, string Dtime, string Stime, string MID, string Reason, string Send)
        {
            try
            {
                Smsevent smsevent = new Smsevent();
                smsevent.From = Send;
                smsevent.To = Dest;
                smsevent.EventType = Reason;
                smsevent.MsgId = MID;
                smsevent.ErrCode = Status;
                smsevent.TagName = Operator + ' ' + Circle;
                smsevent.Udf1 = Dtime;
                smsevent.Udf2 = Stime;
                _repositoryBase.Add<Smsevent>(smsevent);
                _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NoticeDTO InsertReceiveSlack(string eventType, string msgId, string refMessageId, string from, string to, DateTime timestamp, string message)
        {
            try
            {
                if (string.IsNullOrEmpty(msgId))
                    return null;

                var result = _repositoryBase._Context.usp_InsertReceiveSlackAsync(eventType, msgId, refMessageId, from, to, timestamp, message);
                result.Wait();
                List<usp_InsertReceiveSlackResult> data = result.Result;

                if (data != null && data.Count > 0)
                {
                    if (data[0].UserID != null && data[0].NoticeID.HasValue)
                    {
                        NoticeDTO notice = new NoticeDTO();
                        notice.Createdby = data[0].UserID;
                        notice.NoticeDetail = message + "\n --Sent via Slack";
                        notice.ParentID = data[0].NoticeID.ToString();
                        notice.GroupID = data[0].GroupID.ToString();
                       // notice.IsAlreadyExists = data[0].IsAlreadyExists;
                        return notice;
                        //string userAgent = "Webhook";

                        //Dictionary<string, object> postParameters = new Dictionary<string, object>();
                        //postParameters.Add("Createdby", notice.Createdby);
                        //postParameters.Add("NoticeDetail", notice.NoticeDetail + "\n --Sent via Email");
                        //postParameters.Add("ParentId", notice.ParentId);
                        //postParameters.Add("GroupID", notice.GroupID);


                        //HttpWebResponse webResponse = MultiformHelper.MultipartFormPost(apiURL + "api/Notice/AddNotice", apiURL + "api/Auth/Login", userAgent, postParameters, "", "");
                        //// Process response  
                        //StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                        //string returnResponseText = responseReader.ReadToEnd();
                        //webResponse.Close();
                    }
                }

                return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
