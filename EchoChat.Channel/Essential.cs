﻿using EchoChat.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Channel
{
    public class Essentials
    {
        public static ConcurrentBag<ChannelOutput> output = new ConcurrentBag<ChannelOutput>();

        public static ConcurrentBag<ChannelLevelSettings> settings = new ConcurrentBag<ChannelLevelSettings>();

        public static ConcurrentBag<UserChannelData> userChannelDatas = new ConcurrentBag<UserChannelData>();

        public static MessageDetails messageDetails = new MessageDetails();

        public static void PushInOutput(string channel, string userID, string msgID, string error, bool isSuccessful = true, string customUniqueID = "")
        {
            try
            {
                ChannelOutput co = new ChannelOutput
                {
                    ChannelName = channel,
                    UserID = userID,
                    MsgID = msgID,
                    Error = error,
                    IsSuccessful = isSuccessful,
                    CustomUniqueID = customUniqueID
                };
                output.Add(co);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static void Clear()
        {
            try
            {
                output.Clear();
                settings.Clear();
                userChannelDatas.Clear();
                messageDetails = new MessageDetails();
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
    }

    public class ChannelLevelSettings
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string Details { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
        public string Value4 { get; set; }
        public string Value5 { get; set; }
        public string Value6 { get; set; }
        public string Value7 { get; set; }
    }
}
