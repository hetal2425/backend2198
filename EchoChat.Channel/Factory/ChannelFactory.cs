﻿using EchoChat.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Channel.Factory
{
    public class ChannelFactory
    {
        public static BaseChannel GetChannel(string channelName)
        {
            BaseChannel channel = null;
            switch (channelName)
            {
                case ChannelMaster.SMSChannel:
                    channel = new SMSChannel();
                    break;
                case ChannelMaster.EmailChannel:
                    channel = new EmailChannel();
                    break;
                case ChannelMaster.WhatsappChannel:
                    channel = new WhatsappChannel();
                    break;
                case ChannelMaster.WhatsappKarixChannel:
                    channel = new WhatsappKarixChannel();
                    break;
                case ChannelMaster.SlackChannel:
                    channel = new SlackChannel();
                    break;
            }
            return channel;
        }
    }
}
