﻿using EchoChat.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Channel
{
    public interface IBroadcaster
    {
        ConcurrentBag<ChannelOutput> Process(MessageDetails messageDetails, List<string> channels, List<UserChannelData> userChannelDatas, List<ChannelLevelSettings> settings);
    }
}
