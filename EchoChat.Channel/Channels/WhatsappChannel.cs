﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EchoChat.Channel.Factory;
using EchoChat.ChannelModels;
using EchoChat.Constants;
using EchoChat.Helper;
using EchoChat.Models;
using log4net;
using Newtonsoft.Json;

namespace EchoChat.Channel
{
    public class WhatsappChannel : BaseChannel
    {
        private static readonly log4net.ILog logger = LogManager.GetLogger(typeof(WhatsappChannel));

        public string GetToken(string waAPI, string password)
        {
            using (var httpClient = new HttpClient())
            {
                logger.Info(string.Format("GetWAToken -- {0}", waAPI + "users/login"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", password);
                StringContent content = new StringContent(JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

                using (var response = httpClient.PostAsync(waAPI + "users/login", content))
                {
                    response.Wait();
                    var apiResponse = response.Result.Content.ReadAsStringAsync();
                    apiResponse.Wait();
                    WAUsers waResponse = JsonConvert.DeserializeObject<WAUsers>(apiResponse.Result);
                    if (waResponse != null)
                    {
                        return waResponse.users[0].token;
                    }
                    return string.Empty;
                }
            }
        }

        public static bool VerifyWhatsAppNumber(string waAPI, string mobileNo, string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                var contact = new
                {
                    contacts = new string[] { mobileNo },
                    force_check = false
                };


                if (!string.IsNullOrEmpty(mobileNo))
                {
                    using (var httpClient = new HttpClient())
                    {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                        StringContent content = new StringContent(JsonConvert.SerializeObject(contact), Encoding.UTF8, "application/json");

                        using (var response = httpClient.PostAsync(waAPI + "contacts/", content))
                        {
                            response.Wait();
                            var apiResponse = response.Result.Content.ReadAsStringAsync();
                            apiResponse.Wait();
                            ContactResponse waResponse = JsonConvert.DeserializeObject<ContactResponse>(apiResponse.Result);

                            if (waResponse != null)
                            {
                                if (waResponse?.contacts != null && waResponse.contacts.Count > 0)
                                {
                                    if (waResponse?.contacts[0].status == "valid")
                                    {
                                        Thread.Sleep(200);
                                        logger.Info(string.Format("{0} - {1} : valid", waAPI, mobileNo));
                                        return true;
                                    }
                                    else if (waResponse?.contacts[0].status == "processing")
                                    {
                                        Thread.Sleep(600);
                                        logger.Info(string.Format("{0} - {1}: processing", waAPI, mobileNo));
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    logger.Info(string.Format("{0} - {1}: invalid", waAPI, mobileNo));
                    return false;
                }
            }
            logger.Info(string.Format("{0} - {1}: invalid", waAPI, mobileNo));
            return false;
        }

        public override Task Process()
        {
            try
            {
                List<ChannelLevelSettings> appSettings = Essentials.settings.Where(a => a.Key.Equals(SettingMaster.WhatsAppAPI)).ToList();
                if (appSettings != null && appSettings.Count > 0)
                {
                    List<WhatsAPPMessage> whatsAPPMessages = BuildMultipleWAMessage();
                    if (whatsAPPMessages == null || whatsAPPMessages.Count == 0)
                    {
                        logger.Error("Failed to send WhatsAPP Messages");
                        return Task.Delay(0);
                    }

                    logger.Info("appSettings count" + appSettings.Count);
                    int cnt = 0;

                    foreach (ChannelLevelSettings setting in appSettings)
                    {
                        int quota = int.Parse(setting.Value1) / whatsAPPMessages.Count;
                        if (quota == 0)
                        {
                            continue;
                        }

                        logger.Info(string.Format("{0} : {1}", setting.Value, quota));

                        string token = GetToken(setting.Value, setting.Value2);

                        List<UserChannelData> userList = Essentials.userChannelDatas.Skip(cnt).Take(Essentials.userChannelDatas.Skip(cnt).Count() > quota ? quota : Essentials.userChannelDatas.Skip(cnt).Count()).ToList();
                        logger.Info(string.Format("UserChannelData List count : {0} {1}", userList.Count, setting.Value));
                        foreach (var ucd in userList)
                        {
                            logger.Info(string.Format("{0} : {1}", cnt, ucd.MobileNo));

                            if (VerifyWhatsAppNumber(setting.Value, ucd.MobileNo, token))
                            {
                                whatsAPPMessages = BuildMultipleWAMessage();
                                foreach (WhatsAPPMessage wam in whatsAPPMessages)
                                {
                                    WhatsAPPMessage dummyWAM = new WhatsAPPMessage();
                                    dummyWAM = wam;

                                    dummyWAM.to = ucd.CustomDetails_1;
                                    foreach (Component c in dummyWAM.template.components.Where(comp => comp.type == "body"))
                                    {
                                        foreach (Parameter p in c.parameters)
                                        {
                                            p.text = p.text.Replace(@"{UserName}", ucd.DisplayName);
                                        }
                                    }
                                    //DelayTesting(ChannelMaster.WhatsappChannel, ucd.UserID);
                                    Send(token, setting.Value, ucd, dummyWAM);
                                }
                                cnt++;
                            }
                            else
                            {
                                Essentials.PushInOutput(ChannelMaster.WhatsappChannel, ucd.UserID, string.Empty, "Unknown Contact", false);
                            }
                        }
                    }
                }

                return Task.Delay(0);
            }
            catch (Exception ex)
            {
                logger.Error($"WA Process: {ex}");
                throw ex;
            }
        }

        private void Send(string token, string waAPI, UserChannelData ucd, WhatsAPPMessage waMessage)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    string request = JsonConvert.SerializeObject(waMessage);
                    logger.Info("Whatsapp request ---\r\n" + request);
                    request = request.Replace("_namespace", "namespace");

                    StringContent content = new StringContent(request, Encoding.UTF8, "application/json");

                    using (var response = httpClient.PostAsync(waAPI + "messages/", content))
                    {
                        response.Wait();
                        Thread.Sleep(200);
                        var apiResponse = response.Result.Content.ReadAsStringAsync();
                        apiResponse.Wait();
                        WAResponse waResponse = JsonConvert.DeserializeObject<WAResponse>(apiResponse.Result);
                        logger.Info("Whatsapp Send Message API Response  ---\r\n" + apiResponse.Result);

                        if (waResponse != null)
                        {
                            if (waResponse?.messages != null && waResponse.messages.Count > 0)
                            {
                                logger.Info($" MobileNo: {ucd.MobileNo} Message Id: {waResponse.messages[0].id}");
                                Essentials.PushInOutput(ChannelMaster.WhatsappChannel, ucd.UserID, waResponse.messages[0].id, string.Empty);
                                return;
                            }
                        }
                    }
                }

                Essentials.PushInOutput(ChannelMaster.WhatsappChannel, ucd.UserID, string.Empty, "Unknown Error", false);
            }
            catch (Exception ex)
            {
                Essentials.PushInOutput(ChannelMaster.WhatsappChannel, ucd.UserID, string.Empty, ex.Message, false);
                logger.Error($"WA Send: {ex}");
                throw ex;
            }
        }

        private WhatsAPPMessage BuildWAMessage(string fileName, string fileType, bool isMainMsg = true)
        {
            TemplateModel tm = GetTemplate("Msg");

            WhatsAPPMessage waMsg = new WhatsAPPMessage();
            waMsg.to = string.Empty;   // Adding this runtime to perform it faster
            waMsg.type = "template";
            Language language = new Language();
            language.policy = "deterministic";
            language.code = tm.Language;

            Template t = new Template();
            t._namespace = tm.Namespace;
            t.language = language;
            t.name = tm.Name;

            string filePath = AWSHelper.GetPresignedFileURL(fileName);
            List<Component> componentList = new List<Component>();
            Component HeaderComponent = new Component();
            HeaderComponent.type = "header";
            List<Parameter> headerParameterList = new List<Parameter>();
            Parameter fileParam = new Parameter();

            Component bodyComponent = new Component();
            List<Parameter> bodyParameterList = new List<Parameter>();
            bodyComponent.type = "body";

            if (isMainMsg)
            {
                if (!tm.IsWithoutParams)
                {
                    if (tm.IsGeneric)
                    {
                        bodyParameterList.Add(FormatWAParameter(Essentials.messageDetails.SenderName));
                        bodyParameterList.Add(FormatWAParameter(Essentials.messageDetails.Message));
                    }
                    else
                    {
                        bodyParameterList.Add(FormatWAParameter("{UserName}"));
                    }
                }
            }

            if (string.IsNullOrEmpty(fileName))
            {
                if (tm.HeaderPresent)
                {
                    headerParameterList.Add(FormatWAParameter(Essentials.messageDetails.Title));
                }
            }

            if (fileType == EchoFileType.Image)
            {
                fileParam.type = "image";

                image attachment = new image();
                attachment.link = filePath;

                fileParam.image = attachment;
                headerParameterList.Add(fileParam);

                HeaderComponent.parameters = headerParameterList;
                componentList.Add(HeaderComponent);

                tm = GetTemplate("image");
                language.code = tm.Language;
                t.language = language;
                t.name = tm.Name;
            }
            else if (fileType == EchoFileType.Video)
            {
                fileParam.type = "video";

                video attachment = new video();
                attachment.link = filePath;

                fileParam.video = attachment;
                headerParameterList.Add(fileParam);

                HeaderComponent.parameters = headerParameterList;
                componentList.Add(HeaderComponent);

                tm = GetTemplate("video");
                language.code = tm.Language;
                t.language = language;
                t.name = tm.Name;
            }
            else if (fileType == EchoFileType.Document)
            {
                fileParam.type = "document";
                document attachment = new document(fileName);
                attachment.link = filePath; ;

                fileParam.document = attachment;
                headerParameterList.Add(fileParam);

                HeaderComponent.parameters = headerParameterList;
                componentList.Add(HeaderComponent);

                tm = GetTemplate("document");
                language.code = tm.Language;
                t.language = language;
                t.name = tm.Name;
            }

            if (tm.Buttons != null && tm.Buttons.Count > 0)
            {
                int index = 1;
                List<Parameter> btnParameterList = new List<Parameter>();
                foreach (TemplateButtons btn in tm.Buttons)
                {
                    Component btnComponent = new Component();
                    btnComponent.type = "button";
                    btnComponent.sub_type = !string.IsNullOrEmpty(btn.URL) && btn.URL.Equals("Yes") ? "url" : !string.IsNullOrEmpty(btn.URL) ? btn.URL : string.Empty;
                    btnComponent.index = index.ToString();
                    btnParameterList.Add(FormatWAParameter(btn.Name));
                    btnComponent.parameters = btnParameterList;
                    componentList.Add(btnComponent);

                    index++;
                }
            }

            if (tm.HeaderPresent)
            {
                HeaderComponent.parameters = headerParameterList;
                componentList.Add(HeaderComponent);
            }

            bodyComponent.parameters = bodyParameterList;
            componentList.Add(bodyComponent);
            t.components = componentList;
            waMsg.template = t;
            return waMsg;
        }

        private List<WhatsAPPMessage> BuildMultipleWAMessage()
        {
            List<WhatsAPPMessage> waMsgList = new List<WhatsAPPMessage>();
            int msgCnt = 0;
            if (Essentials.messageDetails.Files != null && Essentials.messageDetails.Files.Count > 1)
            {
                foreach (EchoFile ef in Essentials.messageDetails.Files)
                {
                    WhatsAPPMessage waMsg = new WhatsAPPMessage();
                    if (msgCnt == 0)
                    {
                        waMsg = BuildWAMessage(ef.FileName, ef.FileType);
                    }
                    else
                    {
                        waMsg = BuildWAMessage(string.Empty, string.Empty, false);
                    }

                    waMsgList.Add(waMsg);
                    msgCnt++;
                }
            }
            else if (Essentials.messageDetails.Files != null && Essentials.messageDetails.Files.Count > 0 && Essentials.messageDetails.Files[0] != null)
            {
                WhatsAPPMessage waMsg = BuildWAMessage(Essentials.messageDetails.Files[0].FileName, Essentials.messageDetails.Files[0].FileType);
                waMsgList.Add(waMsg);
            }
            else
            {
                WhatsAPPMessage waMsg = BuildWAMessage(string.Empty, string.Empty);
                waMsgList.Add(waMsg);
            }

            return waMsgList;
        }

        private Parameter FormatWAParameter(string text)
        {
            Parameter _param = new Parameter();
            _param.type = "text";
            _param.text = text;
            return _param;
        }

        private TemplateModel GetTemplate(string type)
        {
            TemplateModel templateModel = new TemplateModel();

            List<ChannelLevelSettings> appSettings = Essentials.settings.Where(a => a.Key.Equals(SettingMaster.WhatsAppTemplate) && a.Value1.ToLower() == type.ToLower()).ToList();

            foreach (ChannelLevelSettings cls in appSettings)
            {
                if (!string.IsNullOrEmpty(cls.Value3) && Essentials.messageDetails.Message.Contains(cls.Value3))
                {
                    templateModel.Namespace = cls.Details;
                    templateModel.Name = cls.Value;
                    templateModel.Language = string.IsNullOrEmpty(cls.Value2) ? "en" : cls.Value2;
                    templateModel.HeaderPresent = string.IsNullOrEmpty(cls.Value4) || cls.Value4 == "Yes" ? true : false;
                    templateModel.IsGeneric = string.IsNullOrEmpty(cls.Value5) || cls.Value5 == "Yes" ? true : false;
                    templateModel.IsWithoutParams = !string.IsNullOrEmpty(cls.Value6) && cls.Value6 == "Yes" ? true : false;
                    break;
                }
            }

            if (string.IsNullOrEmpty(templateModel.Name))
            {
                ChannelLevelSettings setting = appSettings.Where(a => a.Value1.ToLower() == type.ToLower() && (a.Value3 == null || a.Value3 == string.Empty)).FirstOrDefault();
                templateModel.Namespace = setting.Details;
                templateModel.Name = setting.Value;
                templateModel.Language = string.IsNullOrEmpty(setting.Value2) ? "en" : setting.Value2;
                templateModel.HeaderPresent = string.IsNullOrEmpty(setting.Value4) || setting.Value4 == "Yes" ? true : false;
                templateModel.IsGeneric = string.IsNullOrEmpty(setting.Value5) || setting.Value5 == "Yes" ? true : false;
                templateModel.IsWithoutParams = !string.IsNullOrEmpty(setting.Value6) && setting.Value6 == "Yes" ? true : false;
            }

            List<ChannelLevelSettings> buttons = Essentials.settings.Where(a => a.Key.Equals(SettingMaster.WhatsAppTemplateButton) && a.Value == templateModel.Name).ToList();

            if (buttons != null && buttons.Count > 0)
            {
                templateModel.Buttons = new List<TemplateButtons>();

                foreach (ChannelLevelSettings btn in buttons)
                {
                    TemplateButtons tButtons = new TemplateButtons();
                    tButtons.ButtonType = btn.Value1;
                    tButtons.URL = btn.Details;
                    tButtons.Name = btn.Value2;
                    tButtons.PhoneNumber = btn.Value3;

                    templateModel.Buttons.Add(tButtons);
                }
            }

            return templateModel;
        }
    }
}
