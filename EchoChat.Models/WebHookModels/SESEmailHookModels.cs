﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models.WebHookModels
{
    public class AWSSESEvent
    {
        public string Type { get; set; }
        public string MessageId { get; set; }
        public string TopicArn { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }
        public string SignatureVersion { get; set; }
        public string Signature { get; set; }
        public string SigningCertURL { get; set; }
        public string UnsubscribeURL { get; set; }
    }















    public class Header
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class CommonHeaders
    {
        public List<string> from { get; set; }
        public string date { get; set; }
        public List<string> bcc { get; set; }
        public string messageId { get; set; }
        public string subject { get; set; }
    }

    public class Tags
    {
        [JsonProperty("ses:operation")]
        public List<string> SesOperation { get; set; }

        [JsonProperty("ses:configuration-set")]
        public List<string> SesConfigurationSet { get; set; }

        [JsonProperty("ses:source-ip")]
        public List<string> SesSourceIp { get; set; }

        [JsonProperty("ses:from-domain")]
        public List<string> SesFromDomain { get; set; }

        [JsonProperty("ses:caller-identity")]
        public List<string> SesCallerIdentity { get; set; }
    }

    public class Mail
    {
        public DateTime timestamp { get; set; }
        public string source { get; set; }
        public string sendingAccountId { get; set; }
        public string messageId { get; set; }
        public List<string> destination { get; set; }
        public bool headersTruncated { get; set; }
        public List<Header> headers { get; set; }
        public CommonHeaders commonHeaders { get; set; }
        public Tags tags { get; set; }
    }

    public class Open
    {
        public DateTime timestamp { get; set; }
        public string userAgent { get; set; }
        public string ipAddress { get; set; }
    }

    public class Delivery
    {
        public DateTime timestamp { get; set; }
        public int processingTimeMillis { get; set; }
        public List<string> recipients { get; set; }
        public string smtpResponse { get; set; }
        public string reportingMTA { get; set; }
    }

    public class AWSSESEventMessege
    {
        public string eventType { get; set; }
        public Mail mail { get; set; }
        public Open open { get; set; }

        public Delivery delivery { get; set; }
    }










    public class ReceivedCommonHeaders
    {
        public string returnPath { get; set; }
        public List<string> from { get; set; }
        public string date { get; set; }
        public List<string> to { get; set; }
        public string messageId { get; set; }
        public string subject { get; set; }
    }

    public class ReceivedMail
    {
        public DateTime timestamp { get; set; }
        public string source { get; set; }
        public string messageId { get; set; }
        public List<string> destination { get; set; }
        public bool headersTruncated { get; set; }
        public List<Header> headers { get; set; }
        public ReceivedCommonHeaders commonHeaders { get; set; }
    }

    public class SpamVerdict
    {
        public string status { get; set; }
    }

    public class VirusVerdict
    {
        public string status { get; set; }
    }

    public class SpfVerdict
    {
        public string status { get; set; }
    }

    public class DkimVerdict
    {
        public string status { get; set; }
    }

    public class DmarcVerdict
    {
        public string status { get; set; }
    }

    public class Action
    {
        public string type { get; set; }
        public string topicArn { get; set; }
        public string encoding { get; set; }
    }

    public class Receipt
    {
        public DateTime timestamp { get; set; }
        public int processingTimeMillis { get; set; }
        public List<string> recipients { get; set; }
        public SpamVerdict spamVerdict { get; set; }
        public VirusVerdict virusVerdict { get; set; }
        public SpfVerdict spfVerdict { get; set; }
        public DkimVerdict dkimVerdict { get; set; }
        public DmarcVerdict dmarcVerdict { get; set; }
        public Action action { get; set; }
    }

    public class AWSReceiveEmailData
    {
        public string notificationType { get; set; }
        public ReceivedMail mail { get; set; }
        public Receipt receipt { get; set; }
        public string content { get; set; }
    }
}
