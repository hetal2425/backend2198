﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class GroupReportHeading
    {
        public int? TotalGroups { get; set; }
        public int? TotalMembers { get; set; }
        public int? TotalNotices { get; set; }
    }

    public class GroupReportDetails
    {
        public int GroupID { get; set; }
        public string Name { get; set; }
        public int MembersCount { get; set; }
        public string CreatedDate { get; set; }
        public string LastMsgDate { get; set; }
        public int? MsgCount { get; set; }
        public string ActivityLevel { get; set; }
    }

    public class GroupReportDetailsModel
    {
        public int Total { get; set; }
        public List<GroupReportDetails> GroupReportDetails { get; set; }
    }

    public class CampaignReportHeading
    {
        public string Channel { get; set; }
    }

    public class CampaignReportDetails
    {
        public int NoticeID { get; set; }
        public string Message { get; set; }
        public string Sender { get; set; }
        public string NoticeDate { get; set; }
        public string FirstChannel { get; set; }
        public string SecondChannel { get; set; }
        public string ThirdChannel { get; set; }
        public string FirstPercentage { get; set; }
        public string SecondPercentage { get; set; }
        public string ThirdPercentage { get; set; }
    }

    public class CampaignReportDetailsModel
    {
        public int Total { get; set; }
        public List<CampaignReportDetails> CampaignReportDetails { get; set; }
    }

    public class ReadReportHeading
    {
        public string NoticeTitle { get; set; }
        public string Owner { get; set; }
        public string NoticeDate { get; set; }
    }

    public class ReadReportDetails
    {
        public string Member { get; set; }
        public string MobileNo { get; set; }
        public string EchoDate { get; set; }
        public string EchoStatus { get; set; }
        public string SMSDate { get; set; }
        public string SMSStatus { get; set; }
        public string EmailDate { get; set; }
        public string EmailStatus { get; set; }
        public string WhatsAppDate { get; set; }
        public string WhatsAppStatus { get; set; }
    }

    public class ReadReportDetailsModel
    {
        public int Total { get; set; }
        public List<ReadReportDetails> ReadReportDetails { get; set; }
    }

    public class ChannelSavingModel
    {
        public string ChannelName { get; set; }
        public int? Valid { get; set; }
        public int ArtificialIntelligence { get; set; }
        public int? Invalid { get; set; }
    }

    public class ChannelInsightModel
    {
        public string ChannelName { get; set; }
        public int Audience { get; set; }
        public int Sent { get; set; }
        public int Delivered { get; set; }
        public int Seen { get; set; }
        public int Responded { get; set; }
        public int OptOut { get; set; }
    }

    public class WeekdaysEngagementModel
    {
        public string ChannelName { get; set; }
        public int Sent { get; set; }
        public int Weekend { get; set; }
        public int Weekday { get; set; }
    }

    public class CampaignSummaryModel
    {
        public int Campaigns { get; set; }
        public int Audience { get; set; }
        public int Groups { get; set; }
        public int MessagesSent { get; set; }
        public int Responses { get; set; }
        public int Engagement { get; set; }
        public int Seen { get; set; }
        public int OptOut { get; set; }
    }

    public class ChannelProgressModel
    {
        public string ChannelName { get; set; }
        public int Q1 { get; set; }
        public int Q2 { get; set; }
        public int Q3 { get; set; }
        public int Q4 { get; set; }
    }

    public class ChannelPredictionModel
    {
        public Guid UserID { get; set; }
        public string Slot { get; set; }
        public string Value { get; set; }
    }
}