﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models
{
    public class MessageDetails
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public string HTMLMessage { get; set; }
        public List<EchoFile> Files { get; set; }
        public string SenderName { get; set; }
        public string SenderDetails { get; set; }
        public string GroupName { get; set; }
        public string CustomDetails_1 { get; set; } // origintorMail
        public string CustomDetails_2 { get; set; } // Notice or Blank
        public string CustomDetails_3 { get; set; } // Notice record JSON string
        public string CustomDetails_4 { get; set; }
        public string CustomDetails_5 { get; set; }        
        public string CustomDetails_6 { get; set; }
        public string CustomDetails_7 { get; set; }
        public string TemplateID { get; set; }
    }

    public class EchoFile
    {
        public string FileName { get; set; }
        public string FileType { get; set; }
    }
}
