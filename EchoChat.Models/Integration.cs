﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class IntegrationOne
    {
        public bool? IsSMS { get; set; }
        public bool? IsEmail { get; set; }
        public bool? IsWhatsApp { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public string DetailsInHTML { get; set; }
        public IFormFile FileObject { get; set; }
        public int? FileType { get; set; }
        public string SenderEmail { get; set; }
        public string SenderName { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public string CustomDetails_1 { get; set; } // WAMobileNo
        public string CustomDetails_2 { get; set; } // DeviceToken
        public string CustomDetails_3 { get; set; } // FacebookID
    }

    public class IntegrationMulltiple
    {
        public bool? IsSMS { get; set; }
        public bool? IsEmail { get; set; }
        public bool? IsWhatsApp { get; set; }
        public List<UserChannelData> userChannelDatas { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public string DetailsInHTML { get; set; }
        public string SenderEmail { get; set; }
        public string SenderName { get; set; }
    }
}