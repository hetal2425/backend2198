﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class GroupDTO
    {
        public int GroupID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string GroupImage { get; set; }
        public Guid CreatedBy { get; set; }
        public int ClientID { get; set; }
        public bool? IsPrivate { get; set; }
    }
}
