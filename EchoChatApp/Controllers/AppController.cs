﻿using EchoChat.Channel;
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class AppController : ControllerBase
    {
        private IAppProvider appProvider;
        private ILog logger;
        public AppController(IAppProvider oAppProvider)
        {
            appProvider = oAppProvider;
            logger = Logger.GetLogger(typeof(AppController));
        }


        [Route("GetAppVersion")]
        [HttpGet]
        public IActionResult GetAppVersion(string Platform)
        {
            try
            {
                string appVersion = appProvider.GetAppVersion(Platform);
                return Ok(appVersion);
            }
            catch (Exception ex)
            {
                logger.Error("GetAppVersion" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("SaveAppVersion")]
        [HttpPost]
        public IActionResult SaveAppVersion(AppVersionDTO appRequest)
        {
            try
            {
                bool result = appProvider.SaveAppVersion(appRequest);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("SaveAppVersion" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

    }

    
}
