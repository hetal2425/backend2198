﻿using EchoChat.Models;
using EchoChat.Models.WebHookModels;
using EchoChat.Repository;
using System;
using System.Threading.Tasks;

namespace EchoChat.ServiceProvider
{
    public class HookProvider : IHookProvider
    {
        IHookRepository hookRepository;
        public HookProvider(IHookRepository oHookRepository)
        {
            hookRepository = oHookRepository;
        }

        public void InsertSMSEvents(string Status, string Operator, string Circle, string Dest, string Dtime, string Stime, string MID, string Reason, string Send)
        {
            try
            {
                hookRepository.InsertSMSEvents(Status, Operator, Circle, Dest, Dtime, Stime, MID, Reason, Send);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NoticeDTO InsertIncomingWhatsApp(WAMessage msg, string apiURL)
        {
            try
            {
                return hookRepository.InsertIncomingWhatsApp(msg, apiURL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertLinkedHelperData(LinkedHelperData data)
        {
            try
            {
                hookRepository.InsertLinkedHelperData(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NoticeDTO InsertReceiveEmail(AWSReceiveEmailData message, string apiURL)
        {
            try
            {
                return hookRepository.InsertReceiveEmail(message, apiURL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertSESEvents(AWSSESEventMessege message)
        {
            try
            {
                hookRepository.InsertSESEvents(message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertWhatsAppEvents(string recipient_id, string id, string status, DateTime _timeStamp)
        {
            try
            {
                hookRepository.InsertWhatsAppEvents(recipient_id, id, status, _timeStamp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NoticeDTO InsertReceiveSlack(string eventType, string msgId, string refMessageId, string from, string to, DateTime timestamp, string message)
        {
            try
            {
                return hookRepository.InsertReceiveSlack(eventType, msgId, refMessageId, from, to, timestamp, message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}