﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailEvent](
	[EmailEventID] [int] IDENTITY(1,1) NOT NULL,
	[EventType] [varchar](20) NULL,
	[MsgID] [varchar](100) NULL,
	[From] [varchar](50) NULL,
	[To] [varchar](50) NULL,
	[OpenByIPAddress] [varchar](50) NULL,
	[Timestamp] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EmailEventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[SMSEvent](
	[SMSEventID] [int] IDENTITY(1,1) NOT NULL,
	[EventType] [varchar](20) NULL,
	[MsgID] [varchar](100) NULL,
	[From] [varchar](50) NULL,
	[To] [varchar](50) NULL,
	[ErrCode] [varchar](50) NULL,
	[TagName] [varchar](50) NULL,
	[Udf1] [varchar](50) NULL,
	[Udf2] [varchar](50) NULL,
	[CreateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[SMSEventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notice]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notice](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NoticeID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[UserID] [uniqueidentifier] NULL,
	[IsRead] [bit] NULL,
	[IsNotified] [bit] NULL,
	[CreateDate] [datetime] NULL,
	[WAMsgID] [varchar](100) NULL,
	[EmailMsgID] [varchar](100) NULL,
	[SMSMsgID] [varchar](100) NULL,
	[AppReadDate] [datetime] NULL,
	[AppNotifyDate] [datetime] NULL,
 CONSTRAINT [PK_Notice] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WhatsappDetails]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WhatsappDetails](
	[WaId] [int] IDENTITY(1,1) NOT NULL,
	[RecipientId] [varchar](20) NULL,
	[MsgId] [varchar](100) NULL,
	[status] [varchar](50) NULL,
	[timestamp] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[WaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[vwChannelActivityLevel]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwChannelActivityLevel] AS

SELECT 
		ISNULL(SUM(ISNULL(CASE WHEN n.IsNotified = 1 THEN 1 ELSE 0 END, 0)),0) +		
		ISNULL(SUM(ISNULL(CASE WHEN se.EventType = 'Delivered' THEN 1 ELSE 0 END, 0)),0) +
		ISNULL(SUM(ISNULL(CASE WHEN ee.EventType = 'Delivery' THEN 1 ELSE 0 END, 0)),0) +
		ISNULL(SUM(ISNULL(CASE WHEN wa.[status] = 'delivered' THEN 1 ELSE 0 END, 0)),0)
		AS DeliveryCount,
		ISNULL(SUM(ISNULL(CASE WHEN n.IsRead = 1 THEN 1 ELSE 0 END, 0)),0) +
		(ISNULL(SUM(ISNULL(CASE WHEN se.EventType = 'Read' THEN 1 ELSE 0 END, 0)),0) +		
		ISNULL(SUM(ISNULL(CASE WHEN ee.EventType = 'Open' THEN 1 ELSE 0 END, 0)),0) +		
		ISNULL(SUM(ISNULL(CASE WHEN wa.[status] = 'Read' THEN 1 ELSE 0 END, 0)),0) )
		AS ReadCount,
		n.GroupID
		FROM Notice n
		LEFT OUTER JOIN [dbo].[SMSEvent] se  ON n.SMSMsgID = se.MsgID
		LEFT OUTER JOIN [dbo].[EmailEvent] ee  ON n.EmailMsgID = ee.MsgID
		LEFT OUTER JOIN [dbo].[WhatsappDetails] wa  ON n.WAMsgID = wa.MsgID
		GROUP BY n.GroupID


GO
/****** Object:  Table [dbo].[NoticeDetails]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NoticeDetails](
	[NoticeID] [int] IDENTITY(1,1) NOT NULL,
	[NoticeTitle] [nvarchar](100) NULL,
	[NoticeDetail] [nvarchar](max) NULL,
	[NoticeDate] [datetime] NOT NULL,
	[GroupID] [int] NULL,
	[ParentID] [int] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[FileName] [nvarchar](150) NULL,
	[FileType] [int] NULL,
	[IsReply] [bit] NULL,
	[LatestMessage] [nvarchar](max) NULL,
	[IsSms] [bit] NULL,
	[IsEmail] [bit] NULL,
	[IsWhatsapp] [bit] NULL,
	[IsFacebook] [bit] NULL,
	[IsViber] [bit] NULL,
	[IsTelegram] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_NoticeDetails] PRIMARY KEY CLUSTERED 
(
	[NoticeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


CREATE TABLE [dbo].[AppSettings](
	[AppSettingID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [varchar](200) NOT NULL,
	[Value] [varchar](max) NULL,
	[Details] [varchar](max) NULL,
	[Value1] [nvarchar](50) NULL,
	[Value2] [nvarchar](50) NULL,
	[Value3] [nvarchar](50) NULL,
	[EntityID] [int] NULL,
	[EntityName] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[AppSettingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppVersion]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppVersion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Platform] [nvarchar](50) NULL,
	[Version] [nvarchar](50) NULL,
	[Date] [date] NULL,
 CONSTRAINT [PK_AppVersion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Channel]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Channel](
	[ChannelID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Channel] PRIMARY KEY CLUSTERED 
(
	[ChannelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChannelData]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChannelData](
	[ChannelDataID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](20) NOT NULL,
	[MsgID] [varchar](100) NOT NULL,
	[Status] [varchar](50) NULL,
	[MsgType] [varchar](50) NOT NULL,
	[Timestamp] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ChannelDataID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClientDeviceMapping]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientDeviceMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NULL,
	[DeviceNo] [varchar](50) NULL,
	[isHostel] [bit] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clients]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationName] [nvarchar](100) NULL,
	[ContactPersonName] [nvarchar](100) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Address] [nvarchar](200) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[MemberCount] [int] NULL,
	[UserAllowed] [nvarchar](50) NULL,
	[UDF1] [nvarchar](50) NULL,
	[UDF2] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[IsTrial] [bit] NOT NULL,
	[GoverningId] [uniqueidentifier] NULL,
	[DashboardCode] [nvarchar](500) NULL,
	[ClientLogo] [varchar](100) NULL,
 CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConnectedUser]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConnectedUser](
	[UserID] [uniqueidentifier] NOT NULL,
	[ConnectionID] [nvarchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DemoRequest]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DemoRequest](
	[RequestId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[MobileNumber] [nvarchar](20) NULL,
	[EmailID] [nvarchar](100) NOT NULL,
	[DateTime] [datetime] NULL,
	[Remark] [nvarchar](max) NULL,
	[PageSource] [nvarchar](500) NULL,
	[Org] [varchar](100) NULL,
	[Date] [varchar](20) NULL,
	[Time] [varchar](10) NULL,
 CONSTRAINT [PK_DemoRequest] PRIMARY KEY CLUSTERED 
(
	[RequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FailedNotices]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FailedNotices](
	[UNID] [int] NULL,
	[Channel] [varchar](50) NULL,
	[Error] [varchar](max) NULL,
	[IsProcessed] [bit] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ProcessedDate] [datetime] NULL,
	[UserID] [uniqueidentifier] NULL,
	[ClientID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FailedNoticesArchive]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FailedNoticesArchive](
	[UNID] [int] NULL,
	[Channel] [varchar](50) NULL,
	[Error] [varchar](max) NULL,
	[IsProcessed] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[ProcessedDate] [datetime] NULL,
	[UserID] [uniqueidentifier] NULL,
	[ClientID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Group]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[GroupID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NOT NULL,
	[GroupImage] [varchar](50) NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedDate] [datetime] NULL,
	[ClientID] [int] NOT NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupList]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupList](
	[GroupListID] [int] IDENTITY(1,1) NOT NULL,
	[GroupID] [int] NOT NULL,
	[ListID] [int] NOT NULL,
 CONSTRAINT [PK_GroupList] PRIMARY KEY CLUSTERED 
(
	[GroupListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupUserAdmin]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupUserAdmin](
	[GroupUserAdminID] [int] IDENTITY(1,1) NOT NULL,
	[GroupID] [int] NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GroupUserAdmin] PRIMARY KEY CLUSTERED 
(
	[GroupUserAdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[List](
	[ListID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedDate] [datetime] NULL,
	[ClientID] [int] NOT NULL,
 CONSTRAINT [PK_List] PRIMARY KEY CLUSTERED 
(
	[ListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NoticeScheduler]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NoticeScheduler](
	[NoticeSchedulerID] [int] IDENTITY(1,1) NOT NULL,
	[NoticeID] [int] NOT NULL,
	[Date] [date] NULL,
	[Time] [time](7) NULL,
	[EndDate] [date] NULL,
	[IsRecursive] [bit] NULL,
	[Days] [varchar](100) NOT NULL,
	[IsExecuted] [bit] NULL,
 CONSTRAINT [PK_NoticeScheduler] PRIMARY KEY CLUSTERED 
(
	[NoticeSchedulerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReceiveEmail]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReceiveEmail](
	[ReceiveEmailID] [int] IDENTITY(1,1) NOT NULL,
	[EventType] [varchar](20) NULL,
	[MsgID] [varchar](100) NULL,
	[ReferenceMsgID] [varchar](100) NULL,
	[From] [varchar](50) NULL,
	[To] [varchar](50) NULL,
	[Timestamp] [datetime] NULL,
	[Content] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[ReceiveEmailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserDeviceTokenMapping]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDeviceTokenMapping](
	[UserID] [uniqueidentifier] NOT NULL,
	[DeviceToken] [nvarchar](300) NOT NULL,
	[Platform] [nvarchar](20) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Custom1] [nvarchar](50) NOT NULL,
	[Custom2] [nvarchar](100) NOT NULL,
	[Custom3] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_UserDeviceTokenMapping] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/****** Object:  Table [dbo].[UserList]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserList](
	[UserListID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[ListID] [int] NOT NULL,
	[IsAdmin] [bit] NULL,
 CONSTRAINT [PK_UserList] PRIMARY KEY CLUSTERED 
(
	[UserListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](100) NULL,
	[LastName] [nvarchar](100) NULL,
	[MobileNo] [nvarchar](20) NULL,
	[EmailID] [nvarchar](100) NULL,
	[DOB] [datetime] NULL,
	[Occupation] [nvarchar](50) NULL,
	[Password] [nvarchar](100) NULL,
	[IsPasswordUpdated] [bit] NULL,
	[CreateDate] [datetime] NULL,
	[ProfilePhoto] [nvarchar](max) NULL,
	[Session] [nvarchar](10) NULL,
	[ClientID] [int] NOT NULL,
	[FacebookID] [varchar](50) NULL,
	[WhatsAppID] [varchar](20) NULL,
	[IsDeleted] [bit] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[IsOnWhatsApp] [bit] NULL,
	[IsOnSMS] [bit] NULL,
	[IsOnEmail] [bit] NULL,
	[IsOnTelegram] [bit] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WhatsappMsg]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WhatsappMsg](
	[WhatsappMsgId] [int] IDENTITY(1,1) NOT NULL,
	[from] [nvarchar](50) NULL,
	[MsgId] [nvarchar](50) NULL,
	[text] [nvarchar](max) NULL,
	[timestamp] [datetime] NULL,
	[type] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[WhatsappMsgId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[AppVersion] ADD  CONSTRAINT [DF_AppVersion_Date]  DEFAULT (GETUTCDATE()) FOR [Date]
GO
ALTER TABLE [dbo].[ClientDeviceMapping] ADD  DEFAULT ((0)) FOR [isHostel]
GO
ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_UDF1]  DEFAULT ('0') FOR [UDF1]
GO
ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_IsTrial]  DEFAULT ((1)) FOR [IsTrial]
GO
ALTER TABLE [dbo].[Clients] ADD  DEFAULT ('') FOR [ClientLogo]
GO
ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_CreateDate]  DEFAULT (GETUTCDATE()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[DemoRequest] ADD  DEFAULT (GETUTCDATE()) FOR [DateTime]
GO
ALTER TABLE [dbo].[FailedNoticesArchive] ADD  CONSTRAINT [DF_FailedNoti_CreatedDate]  DEFAULT (GETUTCDATE()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Group] ADD  CONSTRAINT [DF_Group_CreatedDate]  DEFAULT (GETUTCDATE()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GroupUserAdmin] ADD  CONSTRAINT [DF_GroupUserAdmin_CreatedDate]  DEFAULT (GETUTCDATE()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[List] ADD  CONSTRAINT [DF_List_CreatedDate]  DEFAULT (GETUTCDATE()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Notice] ADD  CONSTRAINT [DF_Notice_GroupID]  DEFAULT ((0)) FOR [GroupID]
GO
ALTER TABLE [dbo].[Notice] ADD  CONSTRAINT [DF_Notice_IsRead]  DEFAULT ((0)) FOR [IsRead]
GO
ALTER TABLE [dbo].[Notice] ADD  CONSTRAINT [DF_Notice_IsNotified]  DEFAULT ((0)) FOR [IsNotified]
GO
ALTER TABLE [dbo].[Notice] ADD  DEFAULT (GETUTCDATE()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[NoticeDetails] ADD  CONSTRAINT [DF_NoticeDetails_NoticeDetails]  DEFAULT ('') FOR [NoticeDetail]
GO
ALTER TABLE [dbo].[NoticeDetails] ADD  CONSTRAINT [DF_NoticeDetails_NoticeDate]  DEFAULT (GETUTCDATE()) FOR [NoticeDate]
GO
ALTER TABLE [dbo].[NoticeDetails] ADD  DEFAULT ((0)) FOR [ParentID]
GO
ALTER TABLE [dbo].[NoticeDetails] ADD  CONSTRAINT [DF_NoticeDetails_FileName]  DEFAULT ('') FOR [FileName]
GO
ALTER TABLE [dbo].[NoticeDetails] ADD  CONSTRAINT [DF_NoticeDetails_FileType]  DEFAULT ((0)) FOR [FileType]
GO
ALTER TABLE [dbo].[NoticeDetails] ADD  DEFAULT ((0)) FOR [IsReply]
GO
ALTER TABLE [dbo].[NoticeDetails] ADD  DEFAULT ((0)) FOR [IsSms]
GO
ALTER TABLE [dbo].[NoticeDetails] ADD  CONSTRAINT [DF_NoticeDetails_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SMSEvent] ADD  CONSTRAINT [df_SMSEvent_CreateDate]  DEFAULT (GETUTCDATE()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[UserDeviceTokenMapping] ADD  CONSTRAINT [DF_UserDeviceTokenMapping_DeviceToken]  DEFAULT ('') FOR [DeviceToken]
GO
ALTER TABLE [dbo].[UserDeviceTokenMapping] ADD  CONSTRAINT [DF_UserDeviceTokenMapping_Platform]  DEFAULT ('') FOR [Platform]
GO
ALTER TABLE [dbo].[UserDeviceTokenMapping] ADD  CONSTRAINT [DF_UserDeviceTokenMapping_CreateDate]  DEFAULT (GETUTCDATE()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[UserDeviceTokenMapping] ADD  CONSTRAINT [DF_UserDeviceTokenMapping_ModifiedDate]  DEFAULT (GETUTCDATE()) FOR [ModifiedDate]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT (newid()) FOR [UserID]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT ('') FOR [FacebookID]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT ('') FOR [WhatsAppID]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT ((0)) FOR [IsOnWhatsApp]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT ((0)) FOR [IsOnSMS]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT ((0)) FOR [IsOnEmail]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT ((0)) FOR [IsOnTelegram]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_CreateDate]  DEFAULT (GETUTCDATE()) FOR [CreateDate]
GO