﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class VwChannelActivityLevel
    {
        public int? DeliveryCount { get; set; }
        public int? ReadCount { get; set; }
        public int GroupId { get; set; }
    }
}
