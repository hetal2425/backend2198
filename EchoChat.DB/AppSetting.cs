﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class AppSetting
    {
        public int AppSettingId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Details { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
        public int? EntityId { get; set; }
        public string EntityName { get; set; }
        public string Value4 { get; set; }
        public string Value5 { get; set; }
        public string Value6 { get; set; }
        public string Value7 { get; set; }
    }
}
