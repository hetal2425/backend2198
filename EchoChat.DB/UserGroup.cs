﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class UserGroup
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public string GroupImage { get; set; }
        public string Owner { get; set; }
        public bool IsRole { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Department { get; set; }
        public string SubDepartment { get; set; }
        public string NoOfClasses { get; set; }
        public int? ClientId { get; set; }
        public int? OverallClasses { get; set; }
        public int? FlowType { get; set; }
        public int AttendanceOption { get; set; }
        public string AttendanceOptionValue { get; set; }
        public int? GraceTime { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
