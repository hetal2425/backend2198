﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class Quotum
    {
        public int QuotaId { get; set; }
        public string Channel { get; set; }
        public int? Usage { get; set; }
        public int? ClientId { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
